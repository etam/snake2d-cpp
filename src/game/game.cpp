/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#include "game.hpp"

#include "../geom2d/direction.hpp"

#include "../stdex/random.hpp"


Game::Game(const geom2d::Size& size)
    : board(size)
    , snake(board.cell_at({5,5}), geom2d::Direction::Right, 6)
    , apple(board.random_empty_cell())
{}

Game::~Game()
{}

bool Game::step()
{
    Board::Cell* next_cell = board.next_cell(snake.head_cell(), snake.dir());
    if (next_cell->what == Board::Cell::Snake || next_cell->what == Board::Cell::Border)
        return false;
    if (next_cell->what == Board::Cell::Apple) {
        snake.grow(stdex::random<unsigned>(1,4));
        ++_score;
        apple.move_to(board.random_empty_cell());
    }
    snake.step(next_cell);
    return true;
}
