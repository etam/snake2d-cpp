/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#include "board.hpp"

Board::Board(const geom2d::Size& size)
    : _size(size)
    , board(size.width * size.height)
{
    for (unsigned y = 0; y < size.height; ++y)
        cell_at({0,y})->what = cell_at({size.width-1,y})->what = Cell::Border;
    for (unsigned x = 1; x < size.width-1; ++x)
        cell_at({x,0})->what = cell_at({x,size.height-1})->what = Cell::Border;
}

auto Board::random_empty_cell() noexcept -> Cell*
{
    do {
        Cell* cell = random_cell();
        if (cell->what == Cell::Empty)
            return cell;
    } while(true);
}
