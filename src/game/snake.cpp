/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#include "snake.hpp"

Snake::Snake(Board::Cell* head, geom2d::Direction dir, unsigned init_length)
    : to_grow(init_length)
    , old_dir(dir)
    , _dir(dir)
{
    step(head);
}

void Snake::step(Board::Cell* cell)
{
    body.push_front(cell);
    cell->what = Board::Cell::Snake;
    if (to_grow)
        --to_grow;
    else {
        body.back()->what = Board::Cell::Empty;
        body.pop_back();
    }
    old_dir = dir();
}
