/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GAME_APPLE_HPP
#define GAME_APPLE_HPP

#include "board.hpp"


class Apple
{
private:
    Board::Cell* cell;

public:
    explicit
    Apple(Board::Cell* _cell) noexcept
        : cell(_cell)
    {
        cell->what = Board::Cell::Apple;
    }

    void move_to(Board::Cell* new_cell) noexcept
    {
        cell->what = Board::Cell::Empty;
        cell = new_cell;
        cell->what = Board::Cell::Apple;
    }
};


#endif // GAME_APPLE_HPP
