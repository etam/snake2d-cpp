/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GAME_BOARD_HPP
#define GAME_BOARD_HPP

#include <vector>

#include "../geom2d/direction.hpp"
#include "../geom2d/point.hpp"
#include "../geom2d/size.hpp"

#include "../stdex/random.hpp"


class Board
{
public:
    struct Cell
    {
        enum What {Empty, Apple, Snake, Border};
        What what = Empty;
    };

private:
    geom2d::Size _size;
    std::vector<Cell> board;

public:
    explicit
    Board(const geom2d::Size& size);

    const geom2d::Size& size() const noexcept
    { return _size; }

    Cell* cell_at(const geom2d::Point& p) noexcept
    { return &board[p.y*_size.width + p.x]; }

    geom2d::Point point(const Cell* cell) const
    {
        unsigned pos = cell - board.data();
        return {pos%size().width, pos/size().width};
    }

    Cell* next_cell(const Cell* cell, geom2d::Direction d)
    { return cell_at(point(cell) + d.to_vector()); }

    Cell* random_cell() noexcept
    { return &board[stdex::random<unsigned>(0, size().height * size().width - 1)]; }

    Cell* random_empty_cell() noexcept;
};


#endif // GAME_BOARD_HPP
