/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GAME_HPP
#define GAME_HPP

#include "../geom2d/size.hpp"

#include "apple.hpp"
#include "board.hpp"
#include "snake.hpp"


class Game
{
public:
    Board board;
    Snake snake;
    Apple apple;

private:
    unsigned _score = 0;

public:
    explicit
    Game(const geom2d::Size& size);

    ~Game();

    unsigned score() const noexcept
    { return _score; }

    void reset()
    {
        const auto size = board.size();
        this->~Game();
        new (this) Game(size);
    }

    bool step();
};


#endif // GAME_HPP
