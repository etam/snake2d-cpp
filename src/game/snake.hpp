/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GAME_SNAKE_HPP
#define GAME_SNAKE_HPP

#include <list>

#include "../geom2d/direction.hpp"

#include "board.hpp"


class Snake
{
private:
    std::list<Board::Cell*> body;
    unsigned to_grow;
    geom2d::Direction old_dir, _dir;

public:
    Snake(Board::Cell* head, geom2d::Direction dir, unsigned init_length);

    geom2d::Direction dir() const noexcept
    { return _dir; }

    void dir(geom2d::Direction new_dir) noexcept
    {
        if (new_dir != old_dir.opposite())
            _dir = new_dir;
    }

    Board::Cell* head_cell() const
    { return body.front(); }

    Board::Cell* next_after_head_cell() const
    { return *std::next(body.begin()); }

    void grow(unsigned how_many) noexcept
    { to_grow += how_many; }

    void step(Board::Cell* cell);
};


#endif // GAME_SNAKE_HPP
