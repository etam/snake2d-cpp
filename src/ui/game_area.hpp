/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GAME_AREA_HPP
#define GAME_AREA_HPP

#include <glibmm/main.h>
#include <gtkmm/drawingarea.h>

namespace geom2d {
class Size;
} // namespace geom2d

#include "../game/game.hpp"


class GameArea
    : public Gtk::DrawingArea
{
private:
    Game game;
    sigc::connection timeout_conn;

public:
    explicit
    GameArea(const geom2d::Size& size);

    ~GameArea();

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

    bool on_key_press_event(GdkEventKey* event);

    void on_show();
    void on_hide();

    bool on_timeout();
    void show_game_over();
};


#endif // GAME_AREA_HPP
