/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#include "game_window.hpp"

GameWindow::GameWindow()
    : game_area({800,600})
{
    set_title("Snake");
    set_resizable(false);
    add(game_area);
    game_area.show();
}

GameWindow::~GameWindow()
{}


bool GameWindow::on_key_press_event(GdkEventKey* event)
{
    switch (event->keyval) {
    case GDK_KEY_Escape:
    case GDK_KEY_q:
        hide();
        return true; // cancel event propagation
    }
    return Gtk::Window::on_key_press_event(event);
}
