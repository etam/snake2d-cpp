/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#include "game_area.hpp"

#include <string>

#include <cairomm/context.h>
#include <gtkmm/messagedialog.h>

#include "../geom2d/size.hpp"


namespace {

constexpr const geom2d::Size pixel_size{10,10};

} // namespace


GameArea::GameArea(const geom2d::Size& size)
    : game(size/pixel_size)
{
    set_size_request(size.width, size.height);
    set_can_focus(true);
    grab_focus();
}

GameArea::~GameArea()
{}


bool GameArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    for (unsigned y = 0; y < game.board.size().height; ++y)
        for (unsigned x = 0; x < game.board.size().width; ++x) {
            switch (game.board.cell_at({x,y})->what) {
            case Board::Cell::Empty:  cr->set_source_rgb(0,0,0); break;
            case Board::Cell::Apple:  cr->set_source_rgb(1,0,0); break;
            case Board::Cell::Snake:  cr->set_source_rgb(0,1,0); break;
            case Board::Cell::Border: cr->set_source_rgb(0,0,1); break;
            }
            cr->rectangle(x*pixel_size.width, y*pixel_size.height, pixel_size.width, pixel_size.height);
            cr->fill();
        }
    return true;
}

bool GameArea::on_key_press_event(GdkEventKey* event)
{
    switch (event->keyval) {
    case GDK_KEY_Up:    game.snake.dir(geom2d::Direction::Up);    break;
    case GDK_KEY_Right: game.snake.dir(geom2d::Direction::Right); break;
    case GDK_KEY_Down:  game.snake.dir(geom2d::Direction::Down);  break;
    case GDK_KEY_Left:  game.snake.dir(geom2d::Direction::Left);  break;
    }
    return Gtk::DrawingArea::on_key_press_event(event);
}

void GameArea::on_show()
{
    Gtk::DrawingArea::on_show();
    timeout_conn = Glib::signal_timeout().connect(sigc::mem_fun(*this, &GameArea::on_timeout), 100);
}

void GameArea::on_hide()
{
    timeout_conn.disconnect();
    Gtk::DrawingArea::on_hide();
}

bool GameArea::on_timeout()
{
    if (!game.step()) {
        show_game_over();
        game.reset();
    }
    queue_draw();
    return true;
}

void GameArea::show_game_over()
{
    Gtk::MessageDialog dialog(dynamic_cast<Gtk::Window&>(*get_toplevel()), "Game Over!");
    dialog.set_secondary_text("Score: " + std::to_string(game.score()));
    dialog.set_modal();
    dialog.run();
}
