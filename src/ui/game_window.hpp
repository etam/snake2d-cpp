/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GAME_WINDOW_HPP
#define GAME_WINDOW_HPP

#include <gtkmm/window.h>

#include "game_area.hpp"


class GameWindow : public Gtk::Window
{
private:
    GameArea game_area;

public:
    GameWindow();

    ~GameWindow();

protected:
    bool on_key_press_event(GdkEventKey* event);
};


#endif // GAME_WINDOW_HPP
