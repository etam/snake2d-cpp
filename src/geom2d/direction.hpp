/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GEOM2D_DIRECTION_HPP
#define GEOM2D_DIRECTION_HPP

#include "vector.hpp"

namespace geom2d {


class Direction
{
public:
    enum enum_type {Up, Right, Down, Left};

private:
    enum_type direction;

public:
    constexpr
    Direction(enum_type _direction) noexcept
        : direction(_direction)
    {}

    constexpr
    Vector to_vector() const noexcept
    { return vectors[static_cast<unsigned>(direction)]; }

private:
    static constexpr Vector vectors[4]{ {0,-1}, {1,0}, {0,1}, {-1,0} };

public:
    constexpr
    Direction opposite() const noexcept
    { return {static_cast<enum_type>( (static_cast<unsigned>(direction)+2) % 4 )}; }

    constexpr
    bool operator==(const Direction& other) const noexcept
    { return direction == other.direction; }

    constexpr
    bool operator!=(const Direction& other) const noexcept
    { return direction != other.direction; }
};


} // namespace geom2d

#endif // GEOM2D_DIRECTION_HPP
