/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GEOM2D_POINT_HPP
#define GEOM2D_POINT_HPP

#include "vector.hpp"

namespace geom2d {


struct Point
{
    unsigned x, y;

    constexpr
    Point operator+(const Vector& v) const noexcept
    { return {x + v.dx, y + v.dy}; }
};


} // namespace geom2d

#endif // GEOM2D_POINT_HPP
