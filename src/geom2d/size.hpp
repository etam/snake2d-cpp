/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#ifndef GEOM2D_SIZE_HPP
#define GEOM2D_SIZE_HPP

namespace geom2d {


struct Size
{
    unsigned width, height;

    constexpr
    Size operator*(unsigned factor) const noexcept
    { return {width*factor, height*factor}; }

    constexpr
    Size operator/(unsigned divisor) const noexcept
    { return {width/divisor, height/divisor}; }

    constexpr
    Size operator*(const Size& other) const noexcept
    { return {width*other.width, height*other.height}; }

    constexpr
    Size operator/(const Size& other) const noexcept
    { return {width/other.width, height/other.height}; }
};


} // namespace geom2d

#endif // GEOM2D_SIZE_HPP
