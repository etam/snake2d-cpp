
//          Copyright Adam Mizerski 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#ifndef STDEX_RANDOM_HPP
#define STDEX_RANDOM_HPP

#include <functional>
#include <limits>
#include <random>

namespace stdex {


template <typename IntType = int>
IntType random(IntType min = 0, IntType max = std::numeric_limits<IntType>::max())
{
    static_assert(std::is_integral<IntType>::value, "template argument not an integral type");
    static auto default_random_engine = std::default_random_engine{ std::random_device{}() };
    static auto uniform_int_distribution = std::uniform_int_distribution<IntType>{};
    static auto generator = std::bind(uniform_int_distribution, default_random_engine, std::placeholders::_1);
    return generator( std::uniform_int_distribution<IntType>(min, max).param() );
}


} // namespace stdex

#endif // STDEX_RANDOM_HPP
