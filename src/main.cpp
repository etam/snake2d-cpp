/*
Copyright © 2000 Adam Mizerski <adam@mizerski.pl>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
*/

#include <gtkmm/application.h>

#include "ui/game_window.hpp"


int main(int argc, char** argv)
{
    auto app = Gtk::Application::create(argc, argv, "org.etam.snake2d-cpp");
    GameWindow game_window;
    game_window.show();
    return app->run(game_window);
}
