snake2d-cpp
===========

Object Oriented Snake2d - C++ version.

Released under the terms of Do What The Fuck You Want To Public License,
with the exception of stdex/random.hpp, which is part of stdex library (https://github.com/etam/stdex) and released under the Boost Software License.

Goals:
- Create simple Snake game in C++11
- Be as much Object Oriented as possible
- To create a nice piece of code (no dirty hacks or ugly workarounds)

Requirements:
- c++11 capable compiler
- gtkmm3
